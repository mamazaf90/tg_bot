import telebot

from telebot.types import (
    InlineKeyboardButton,
    InlineKeyboardMarkup,
    ReplyKeyboardMarkup
)


TOKEN = "6497122078:AAF7KQBVxJD-AdlP3QT-BeJxkm-nUmgqu7Q"


bot = telebot.TeleBot(TOKEN)

users = {}

@bot.message_handler(commands=['start'])
def send_welcome(message):
    bot.reply_to(message, """\
Hi there, I am EchoBot.
I am here to echo your kind words back to you. Just say anything nice and I'll say the exact same thing to you!\
""")
    

@bot.message_handler(commands=['help'])
def help_message_handler(message):
    keyboard = ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
    keyboard.add('💸Прайс', '✅ В наличие', '🛍Магазин')
    keyboard.add('Как купить ⁉️ инструкция', '👉Пополнить btc/ltc')
    keyboard.add('👤Профиль/баланс', '🛎 Ненаход/Правила')
    keyboard.add('⭐️ ПРЕДЗАКАЗ ⭐️')
    bot.reply_to(message, "Вам надо идти налево", reply_markup=keyboard)

@bot.message_handler(commands=["save_me"])
def save_user_info(message):
    msg = bot.send_message(chat_id=message.chat.id, text="Отправьте свое имя")
    bot.register_next_step_handler(msg, save_name)

@bot.message_handler(commands=["who_i"])
def get_user_info(message):
    user_data = users[message.chat.id]
    bot.send_message(chat_id=message.chat.id, text=f"Ваше имя {user_data['name']} {user_data['surname']}")

@bot.message_handler(content_types=["text"])
def main_message_handler(message):
    if message.text == "💸Прайс":
        bot.send_message(chat_id=message.chat.id, text="Стоимость нашей услуги 10 000")
    elif message.text == "Как купить ⁉️ инструкция":
        bot.send_message(chat_id=message.chat.id, text="перейдите на ссылке https://www.youtube.com/")
    elif message.text == "👉Пополнить btc/ltc":
        markup = InlineKeyboardMarkup()
        markup.row_width = 2
        markup.add(
            InlineKeyboardButton("Yes", callback_data="cb_yes"),
            InlineKeyboardButton("No", callback_data="cb_no"),
            InlineKeyboardButton("Дай мне что-то", callback_data="cb_name")
            )
        bot.send_message(chat_id=message.chat.id, text="Это какой-то интересный текст", reply_markup=markup)
    elif message.text == "⭐️ ПРЕДЗАКАЗ ⭐️":
        markup = InlineKeyboardMarkup()
        markup.row_width = 2
        markup.add(
            InlineKeyboardButton("Штаны", callback_data="cb_jeans"),
            InlineKeyboardButton("футболка", callback_data="cb_T-shirt"),
        )
        bot.send_message(chat_id=message.chat.id, text="Что вы хотите заказать?", reply_markup=markup)

@bot.callback_query_handler(func=lambda call: True)
def callback_handler(call):
    if call.data == "cb_jeans":
        bot.send_message(chat_id=call.message.chat.id, text="штаны")
    elif call.data == "cb_T-shirt":
        bot.send_message(chat_id=call.message.chat.id, text="футболка")
        

@bot.callback_query_handler(func=lambda call: True)
def callback_handler(call):
    if call.data == "cb_yes":
        bot.send_message(chat_id=call.message.chat.id, text="Это ответ на 'yes'")
    elif call.data == "cb_no":
        bot.answer_callback_query(call.id, text="Это ответ на ваш отказ")
    elif call.data == "cb_name":
        bot.send_message(chat_id=call.message.chat.id, text="Это ваше Имя Фимилия")

def save_name(message):
    users[message.chat.id] = {
        "name" : message.text
        }
    msg = bot.send_message(message.chat.id, text="Отправьте нам свою фамилию")
    bot.register_next_step_handler(msg, save_surname)

def save_surname(message):
    users[message.chat.id]["surname"] = message.text
    bot.send_message(chat_id=message.chat.id, text="Ваши данные мы сохранили...")


bot.infinity_polling()











